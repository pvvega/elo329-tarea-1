public class Individuo {
    private double x, y, speed, angle, deltaAngle ;
    private double x_tPlusDelta, y_tPlusDelta ;
    private Comuna comuna ;

    public Individuo (Comuna comuna, double speed, double deltaAngle){
	    this.comuna = comuna ;
	    this.speed = speed ;
	    this.deltaAngle = deltaAngle ;
        angle = Math.random()*2*Math.PI ;
    }

    public static String getStateDescription() {
        return "x,\ty" ;
    }

    public String getState() {
        return x + ",\t" + y ;
    }

    public void computeNextState(double delta_t) {
        double r = Math.random() ;
        angle += Math.random()*2*Math.PI ;
        x_tPlusDelta = x+speed*Math.cos(angle) ;
        y_tPlusDelta = y+speed*Math.sin(angle) ;
        if(x_tPlusDelta < 0) {   // rebound logic

        }
        else if(x_tPlusDelta > comuna.getWidth()) {

        }
        if(y_tPlusDelta < 0) {

        }
        else if(y_tPlusDelta > comuna.getHeight()) {

        }
    }

    public void updateState() {
	//??
    }
}
