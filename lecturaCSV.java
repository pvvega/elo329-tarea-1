import java.io.BufferedReader; //tengo el grafico a medias logre hacer que lellera el datos.txt
import java.io.FileReader;     //para obtener cuantos segundos esta la simulacion para crear los arreglos
import javax.swing.JOptionPane;//que contienen las coordenadas los cuales faltaria poner los datos en el grafico de dispercion
public class lecturaCSV {
    private int j = 0;
    private int k = 0;
    private int segundos;
    private BufferedReader lector;
    private String linea;
    private String partes[] = null;

    public void leerArchivo(String nombreArchivo, String nombredatos) {
        try {
            lector = new BufferedReader(new FileReader(nombredatos));
            linea = lector.readLine();
            partes = linea.split(" ");
            segundos = Integer.parseInt(partes[0]);
            lector.close();
            linea = null;
            partes = null;
            double[] coordenadax = new double[segundos];
            double[] coordenaday = new double[segundos];

            lector = new BufferedReader(new FileReader(nombreArchivo));
            while ((linea = lector.readLine()) != null) {
                partes = linea.split(",");

                    for (int i = 0; i< partes.length;i++){
                        if(j>3 && (i>0 && i<2)) {
                            double N=Double.parseDouble(partes[i]);
                            coordenadax[k]=N;
                        }
                        if(j>3 && (i>1 && i<3)) {
                            double M=Double.parseDouble(partes[i]);
                            coordenaday[k]=M;
                            ++k;
                        }
                    }
                ++j;
            }
            lector.close();
            linea = null;
            partes = null;


        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }
}